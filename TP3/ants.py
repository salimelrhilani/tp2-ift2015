# Salim, El Rhilani, 20148191
# Muxue, Guo, 20160375

# import time

import numpy as np
import random as rand


class Colony:
    class Ant:
        def __init__(self, colony):

            self.colony = colony

            self.pos = rand.randrange(self.colony.n)

            self.mem = np.zeros(self.colony.n)
            self.mem[self.pos] = 1

            self.path = [self.pos]

            self.cost = 0

        def reset(self, colony):
            self.__init__(colony)

        def __str__(self):
            return str(self.path) + ", cost : " + str(self.cost)

        def __lt__(self, other):
            return self.cost < other.cost

        # Returns city to be travelled to from current position
        def policy(self):
            # rand.random() returns random number between 0 and 1
            if rand.random() <= self.colony.q_0:
                vertices = {}

                # Choose from candidate list
                for v in self.colony.candidate_list[self.pos]:
                    if self.mem[v] == 0:
                        vertices[v] = \
                            (self.colony.tau[self.pos][v]) * \
                            (self.colony.eta(self.pos, v) ** self.colony.beta)

                if len(vertices) > 0:
                    return max(vertices, key=vertices.get)

                # No adequate vertex in candidate list, choose from all vertices
                for y in range(self.colony.n):
                    if self.mem[y] == 0:
                        vertices[y] = \
                            (self.colony.tau[self.pos][y]) * \
                            (self.colony.eta(self.pos, y) ** self.colony.beta)
                # Choose vertex with most pheromone and min distance (max eta)
                return max(vertices, key=vertices.get)

            else:
                # Stochastic decision
                probabilities = []
                total_prob = 0
                # total_prob represents the value in the denominator of our
                # probability

                for i in self.colony.candidate_list[self.pos]:
                    if self.mem[i] == 0:
                        prob = \
                            (self.colony.tau[self.pos][i]) * \
                            (self.colony.eta(self.pos, i) ** self.colony.beta)
                        total_prob += prob
                        probabilities.append((i, prob))

                # If there is at least one possible destination in candidate list
                if total_prob != 0:

                    # Dividing all probabilities by total_prob is
                    # equivalent to multiplying random_num by total_prob
                    random_num = rand.random() * total_prob
                    vertex = self.pos

                    # Choose vertex according to random number generated
                    for prob in probabilities:
                        if random_num < prob[1]:
                            vertex = prob[0]
                            break
                        else:
                            random_num -= prob[1]

                    if vertex == self.pos:
                        vertex = probabilities[-1]

                    return vertex

                # No possible vertex in candidate list, pick from all vertices
                for i in range(self.colony.n):
                    if self.mem[i] == 0:
                        prob = \
                            (self.colony.tau[self.pos][i]) * \
                            (self.colony.eta(self.pos, i) ** self.colony.beta)
                        total_prob += prob
                        probabilities.append((i, prob))

                # Dividing all probabilities by total_prob is
                # equivalent to multiplying random_num by total_prob
                random_num = rand.random() * total_prob
                vertex = self.pos

                # Choose vertex according to random number generated
                for prob in probabilities:
                    if random_num < prob[1]:
                        vertex = prob[0]
                        break
                    else:
                        random_num -= prob[1]

                if vertex == self.pos:
                    vertex = probabilities[-1]

                return vertex

        # Updates the local pheromones and position of ant
        # while keeping track of total cost and path
        def move(self):
            destination = self.policy()

            # local updating
            new_tau = (1 - self.colony.alpha) * \
                      self.colony.tau[self.pos][destination] + \
                      self.colony.alpha * self.colony.tau_0
            # Update for both edge (r,s) and (s,r) because we have symmetrical
            # graph
            self.colony.tau[self.pos][destination] = new_tau
            self.colony.tau[destination][self.pos] = new_tau

            # Change position
            self.cost += self.colony.adjMat[self.pos][destination]
            self.pos = destination
            self.path.append(destination)
            self.mem[destination] = 1

        # Updates the pheromone levels of ALL edges that form
        # the minimum cost loop at each iteration
        def globalUpdate(self):
            try:
                delta_tau = 1 / self.cost
            except ZeroDivisionError:
                delta_tau = 0
            for i in range(self.colony.n):
                j = (i + 1) % self.colony.n

                city1 = self.path[i]
                city2 = self.path[j]

                new_tau = (1 - self.colony.alpha) * self.colony.tau[city1][city2] + \
                          self.colony.alpha * delta_tau
                # Update for both (r,s) and (s,r) because we have symmetrical
                # graph
                self.colony.tau[city1][city2] = new_tau
                self.colony.tau[city2][city1] = new_tau

            self.colony.transcript.write(str(self) + "\n")
            print(self)

    def __init__(self, adjMat, m=10, beta=2, alpha=0.1, q_0=0.9, cl=15):
        # Parameters:
        # m => Number of ants
        # beta => Importance of heuristic function vs pheromone trail
        # alpha => Updating propensity
        # q_0 => Probability of making a non-stochastic decision
        # tau_0 => Initial pheromone level
        # cl => number of vertex in candidate list

        self.transcript = open('bonus.txt', 'w', encoding='utf-8')

        self.adjMat = adjMat
        self.n = len(adjMat)

        heuristic_cost = self.nearestNearbourHeuristic()
        self.transcript.write("Nearest Nearbour Heuristic Cost : " + str(heuristic_cost) + "\n")
        print("Nearest Nearbour Heuristic Cost : " + str(heuristic_cost))
        self.tau_0 = 1 / (self.n * heuristic_cost)
        self.tau = [[self.tau_0 for _ in range(self.n)] for _ in range(self.n)]
        self.ants = [self.Ant(self) for _ in range(m)]
        self.beta = beta
        self.alpha = alpha
        self.q_0 = q_0
        self.cl = cl
        self.candidate_list = self.makeCandidateList()

    def __str__(self):
        ascii_art = ""
        for i in range(len(self.ants)):
            ascii_art += "      __ \n" + ":@.o.(__) \n " + "/  |  \\  \n"
        return ascii_art

    # Returns the cost of the solution produced by
    # the nearest neighbour heuristic
    def nearestNearbourHeuristic(self):
        costs = np.zeros(self.n)

        for i in range(0, self.n):
            current_pos = i
            mem = np.zeros(self.n)
            mem[i] = 1
            path = [i]
            cost = 0
            for _ in range(self.n - 1):
                min_edge_cost = -1
                next_visited = -1
                for j in range(0, self.n):
                    # Analyse vertices that are not visited
                    if mem[j] == 0:
                        edge_cost = self.adjMat[current_pos][j]

                        # Update min_edge_cost if we have smaller cost or no
                        # other cost
                        if min_edge_cost > edge_cost or min_edge_cost == -1:
                            min_edge_cost = edge_cost
                            next_visited = j

                path.append(next_visited)
                mem[next_visited] = 1
                current_pos = next_visited
                cost += min_edge_cost

            # Complete the cycle
            cost += self.adjMat[path[-1]][i]
            costs[i] = cost

        return min(costs)

    # Create candidate list with size self.cl using self.adjMaj
    # Returns a Python dictionary with an array of vertices associated with
    # each vertex
    def makeCandidateList(self):
        candidate_list = {}
        for vertex in range(self.n):
            vertex_cost_candidate_list = []
            for destination in range(self.n):
                if destination != vertex:
                    cost = adjMat[vertex][destination]
                    vertex_cost_candidate_list.append((cost, destination))
                    vertex_cost_candidate_list.sort()

                    if len(vertex_cost_candidate_list) > self.cl:
                        vertex_cost_candidate_list.pop()

            candidate_list[vertex] = \
                [dst[1] for dst in vertex_cost_candidate_list]

        return candidate_list

    # Heuristic function
    # Returns inverse of smallest distance between r and u
    def eta(self, r, u):
        # Since we have a complete graph with triangular inequality respected,
        # we can assume adjMat[r][u] is the smallest distance
        if adjMat[r][u] == 0:
            return 0
        else:
            return 1 / (adjMat[r][u])

    def optimize(self, num_iter):

        for i in range(num_iter):
            print("Iter: " + str(i))
            for _ in range(self.n - 1):
                for ant in self.ants:
                    ant.move()

            # Complete the cycle
            for ant in self.ants:
                ant.cost += self.adjMat[ant.path[-1]][ant.path[0]]

            min_ant = min(self.ants)

            min_ant.globalUpdate()

            if min_ant.cost < 16000:
                self.transcript.close()
                break

            for ant in self.ants:
                ant.reset(self)


if __name__ == "__main__":
    rand.seed(420)

    file = open('d198.csv')
    # file = open('dantzig.csv')

    adjMat = np.loadtxt(file, delimiter=",")

    ant_colony = Colony(adjMat)
    # t1 = time.time()
    ant_colony.optimize(1000000)
    # print(time.time() - t1)
