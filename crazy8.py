# Salim, Elrhilani, 20148191
# Muxue, Guo, 20160375

import math
import random
import copy


class LinkedList:
    class _Node:
        def __init__(self, v, n=None):
            self.value = v
            self.next = n

    def __init__(self):
        self._head = None
        self._size = 0

    def __str__(self):
        array = "["
        node = self._head

        # Empty List
        if node is None:
            return "[]"

        while node is not None:
            array += str(node.value) + ', '
            node = node.next

        array = array[0:-2]
        array += "]"
        return array

    def __len__(self):
        return self._size

    def isEmpty(self):
        return self._head is None

    # Adds a node of value v to the beginning of the list
    def add(self, v):
        node = self._Node(v)
        first_node = self._head
        self._head = node
        node.next = first_node
        self._size += 1
        return

    # Adds a node of value v to the end of the list
    def append(self, v):
        node = self._Node(v)

        # Append to empty list
        if self._head is None:
            self._head = node
            self._size = 1
            return

        current_node = self._head

        while current_node.next is not None:
            current_node = current_node.next

        current_node.next = node
        self._size += 1

    # Removes and returns the first node of the list
    def pop(self):

        if self._size == 0:
            raise IndexError("pop from empty list")

        first_node = self._head
        self._head = first_node.next
        first_node.next = first_node
        self._size -= 1
        return first_node.value

    # Returns the value of the first node of the list
    def peek(self):
        return self._head.value

    # Removes the node of the list with value v and return v
    def remove(self, v):
        if self._size == 0:
            raise IndexError("remove from empty list")

        current_node = self._head

        # If node to remove is the first one
        if current_node.value == v:
            self._head = current_node.next
            current_node.next = current_node
            self._size -= 1
            return current_node.value

        # Else we walk through the list
        previous_node = self._head
        current_node = self._head.next

        while current_node is not None:
            if current_node.value == v:
                previous_node.next = current_node.next
                current_node.next = current_node
                self._size -= 1
                return current_node.value

            previous_node = current_node
            current_node = current_node.next

        return None


class CircularLinkedList(LinkedList):
    def __init__(self):
        super().__init__()

    def __str__(self):
        if self.isEmpty():
            return "[]"

        txt = "["
        for i in self.__iter__():
            txt += str(i) + ", "

        txt = txt[0:-2]
        txt += "]"
        return txt

    def __iter__(self):
        if self.isEmpty():
            return None
        else:
            pointer = self._head
            for i in range(0, self._size):
                yield pointer.value
                pointer = pointer.next

    # Moves head pointer to next node in list
    def next(self):
        if self._size > 1:
            self._head = self._head.next

    # Adds a node of value v to the end of the list
    def append(self, v):
        # List is empty, add node as head
        if self.isEmpty():
            self._head = self._Node(v)
            self._head.next = self._head
            self._size = 1
        else:
            new_node = self._Node(v, self._head)

            # Find old last node
            old_last_node = self._head

            while old_last_node.next != self._head:
                old_last_node = old_last_node.next

            old_last_node.next = new_node
            self._size += 1

    # Reverses the next pointers of all nodes to previous node
    def reverse(self):
        if self._size <= 2:
            return

        previous_node = self._head
        current_node = previous_node.next
        next_node = current_node.next
        for i in range(0, self._size):
            current_node.next = previous_node
            previous_node = current_node
            current_node = next_node
            next_node = current_node.next

    # Removes head node and returns its value
    def pop(self):
        if self.isEmpty():
            raise IndexError("pop from empty list")
        elif self._size == 1:
            value = self._head.value
            self._head = None
            self._size = 0
            return value
        else:
            # Find last node
            last_node = self._head
            while last_node.next != self._head:
                last_node = last_node.next

            new_head_node = self._head.next
            head_node_value = self._head.value
            self._head.next = self._head

            last_node.next = new_head_node
            self._head = new_head_node
            self._size -= 1
            return head_node_value


class Card:
    def __init__(self, r, s):
        self._rank = r
        self._suit = s

    suits = {'s': '\U00002660', 'h': '\U00002661',
             'd': '\U00002662', 'c': '\U00002663'}

    def __str__(self):
        return self._rank + self.suits[self._suit]

    def __eq__(self, other):
        card_ranks = [self._rank, other._rank]

        # Conversion of J, Q and K is also handled in case one day there would
        # be a game of crazy13
        for i in range(len(card_ranks)):
            if card_ranks[i] == '1':
                card_ranks[i] = 'A'
            elif card_ranks[i] == '11':
                card_ranks[i] = 'J'
            elif card_ranks[i] == '12':
                card_ranks[i] = 'Q'
            elif card_ranks[i] == '13':
                card_ranks[i] = 'K'

        return card_ranks[0] == card_ranks[1] and self._suit == other._suit


class Hand:
    def __init__(self):
        self.cards = {'s': LinkedList(), 'h': LinkedList(),
                      'd': LinkedList(), 'c': LinkedList()}

    def __str__(self):
        result = ''
        for suit in self.cards.values():
            result += str(suit)
        return result

    def __getitem__(self, item):
        return self.cards[item]

    def __len__(self):
        result = 0
        for suit in list(self.cards):
            result += len(self.cards[suit])

        return result

    def add(self, card):
        self.cards[card._suit].add(card)

    def get_most_common_suit(self):
        return max(list(self.cards), key=lambda x: len(self[x]))

    # Returns a card included in the hand according to
    # the criteria contained in *args and None if the card
    # isn't in the hand. The tests show how *args must be used.
    def play(self, *args):
        suits = ['s', 'h', 'd', 'c']

        # Case if we have 2 args
        if len(args) == 2:

            # Assume args = suit, rank
            suit = args[0]
            rank = args[1]

            # If args = rank, suit
            if args[1] in suits:
                rank = args[0]
                suit = args[1]

            card = Card(rank, suit)

            if self.cards[suit].isEmpty():
                return None

            # List not empty we can try to remove
            else:
                val = self.cards[suit].remove(card)
                # Returns None if card not removed
                # and the card if it is removed
                return val

        elif len(args) == 1:
            # Case: Letter only
            if args[0] in suits:
                val = self.cards[args[0]]
                if not val.isEmpty():
                    return val.pop()
                else:
                    return None

            # Case : Rank only
            else:
                for suit in suits:
                    rank = args[0]
                    card = Card(rank, suit)
                    if not self.cards[suit].isEmpty():
                        val = self.cards[suit].remove(card)
                        if val is not None:
                            return val

                # No card found in any suit
                return None


class Deck(LinkedList):
    def __init__(self, custom=False):
        super().__init__()
        if not custom:
            # for all suits
            for i in range(4):
                # for all ranks
                for j in range(13):
                    s = list(Card.suits)[i]
                    r = ''
                    if j == 0:
                        r = 'A'
                    elif j > 0 and j < 10:
                        r = str(j + 1)
                    elif j == 10:
                        r = 'J'
                    elif j == 11:
                        r = 'Q'
                    elif j == 12:
                        r = 'K'
                    self.add(Card(r, s))

    def draw(self):
        return self.pop()

    def shuffle(self, cut_precision=0.05):

        if self.__len__() < 2:
            return

        # Cutting the two decks in two
        center = len(self) / 2
        k = round(random.gauss(center, cut_precision * len(self)))

        if k < 0:
            k = 0

        # other_deck must point the kth node in self
        # (starting at 0 of course)

        # Find node k-1
        current_card = self._head
        for i in range(0, k - 1):
            current_card = current_card.next

        # Separate lists
        other_deck = current_card.next
        current_card.next = None

        # Merging the two decks together
        if random.uniform(0, 1) < 0.5:
            # Switch self._head and other_deck pointers
            temp_deck = other_deck
            other_deck = self._head
            self._head = temp_deck

        self_deck_pointer = self._head
        other_deck_pointer = other_deck
        self_deck_next_pointer = self_deck_pointer.next
        other_deck_next_pointer = other_deck_pointer.next

        while self_deck_next_pointer is not None and \
                other_deck_next_pointer is not None:
            # Merge one card from other_deck into self deck
            self_deck_pointer.next = other_deck_pointer
            other_deck_pointer.next = self_deck_next_pointer

            # Next iteration
            self_deck_pointer = self_deck_next_pointer
            other_deck_pointer = other_deck_next_pointer
            self_deck_next_pointer = self_deck_pointer.next
            other_deck_next_pointer = other_deck_pointer.next

        if self_deck_next_pointer is None:
            # End of self deck, the deck continues with what remains in
            # other_deck (1 or more cards depending on whether
            # other_deck_next_pointer is also None)
            self_deck_pointer.next = other_deck_pointer
        else:
            # Self deck has other cards but we reach the end of other_deck,
            # we add the last card from other_deck before continuing with self
            # deck
            self_deck_pointer.next = other_deck_pointer
            other_deck_pointer.next = self_deck_next_pointer


class Player:
    def __init__(self, name, strategy='naive'):
        self.name = name
        self.score = 8
        self.hand = Hand()
        self.strategy = strategy

    def __str__(self):
        return self.name

    # This function must modify the player's hand,
    # the discard pile, and the game's declared_suit
    # attribute. No other variables must be changed.
    # The player's strategy can ONLY be based
    # on his own cards, the discard pile, and
    # the number of cards his opponents hold.

    # strategy :
    # If we must draw cards , play 2 or Qs
    # Else we play the first card which has the same suit of the actual suit
    # and also different than the actual frime card.
    # if we have no available cards with same suit , play a card with same
    # rank (don't forget priority)
    # else we play frime card and choose a new suits (see the frequence)
    # Finally when we play a card, modify the player's hand and the discard pile
    # If we can't play something we return the game unchanged
    def play(self, game):

        # Card the player chooses to play
        card = None

        top_card = game.discard_pile.peek()

        # If we aren't forced to play a suit
        no_declared_suit = game.declared_suit == ''

        # List of frimes, not used in naive strategy
        frimes = LinkedList()

        if self.strategy == 'naive':

            # Case : no special cards
            if game.draw_count == 0:
                # Try to play according to suit
                if no_declared_suit:
                    card = self.hand.play(top_card._suit)

                # Case : precedent player left the game with a frime, next
                # player can play the card he wants
                elif game.declared_suit == '*':

                    # Unfortunately get_most_common_suit counts the frimes when
                    # counting, so we could have the case where 8s (frime) is
                    # picked in a deck of [8s][7h][6d][5c]
                    # This is handled after when checking if frime is necessary
                    temp_suit = self.hand.get_most_common_suit()
                    card = self.hand.play(temp_suit)

                # Case : we have to play the declared suit
                else:
                    card = self.hand.play(game.declared_suit)

                # Case : No card of corresponding suit, try to play according
                # to rank
                if card is None and game.declared_suit == '':
                    card = self.hand.play(top_card._rank)

                # Play frime
                if card is None:
                    card = self.hand.play(str(self.score))

                    if card is None:
                        return game

                # Case : frime card played, check if it was necessary
                if card._rank == str(self.score) \
                        or card._rank == 'A' and self.score == 1:

                    frime_card = card

                    if no_declared_suit:
                        # The list isn't empty so we can play a different card
                        # of this suit, we add the frime card to the list and
                        # we play the next one instead.
                        if card._suit == top_card._suit and \
                                not self.hand[card._suit].isEmpty():
                            card = self.hand.play(card._suit)
                            self.hand.add(frime_card)

                        # Frime played because it was the only card that
                        # matched top card suit, try to play by rank
                        elif top_card._rank != str(self.score):
                            card = self.hand.play(top_card._rank)
                            if card is None:

                                # Play a frime card in order of suit, and not
                                # according to top card suit
                                self.hand.add(frime_card)

                                card = self.hand.play(str(self.score))
                            else:
                                # Frime was not necessary, put it back
                                self.hand.add(frime_card)
                        # Else frime card played because no card that matches
                        # suit and rank of top card is rank of frime, nothing
                        # else we can do to avoid playing frime

                    elif game.declared_suit == '*':
                        frime_cards = LinkedList()
                        frime_cards.append(frime_card)

                        # Try to find another card we can play
                        while len(self.hand) > 0:
                            temp_suit = self.hand.get_most_common_suit()
                            card = self.hand.play(temp_suit)

                            # Successfully found another card
                            if not (card._rank == str(self.score)
                                    or card._rank == 'A' and self.score == 1):
                                # Place the frimes back in hand
                                while not frime_cards.isEmpty():
                                    self.hand.add(frime_cards.pop())

                                break

                            # Next possible card is also frime, continue
                            # searching
                            else:
                                frime_cards.append(card)

                        # No other choice aside from frime carda
                        if len(self.hand) == 0:
                            # Play the frime in suit order
                            card = frime_cards.remove(
                                Card(str(self.score), 's'))
                            if card is None:
                                card = frime_cards.remove(
                                    Card(str(self.score), 'h'))
                            if card is None:
                                card = frime_cards.remove(
                                    Card(str(self.score), 'd'))
                            if card is None:
                                card = frime_cards.remove(
                                    Card(str(self.score), 'c'))

                            # Put back the others
                            while not frime_cards.isEmpty():
                                self.hand.add(frime_cards.pop())

                    else:
                        # The list isn't empty so we can play a different card of
                        # this suit, we add the frime card to the list and we play
                        # the next one instead.
                        if frime_card._suit == game.declared_suit and \
                                not self.hand[frime_card._suit].isEmpty():
                            card = self.hand.play(frime_card._suit)
                            self.hand.add(frime_card)

            # Case : we have to play 2 or Qs, we prioritize 2 unless 2 is frimed
            else:
                if no_declared_suit:
                    if self.score == 2:
                        card = self.hand.play('Q', 's')

                        # Case : no Qs card found, we have to play 2
                        if card is None:
                            card = self.hand.play('2')
                    else:
                        card = self.hand.play('2')

                        # Case : no 2 found, we have to play Qs
                        if card is None:
                            card = self.hand.play('Q', 's')

                elif game.declared_suit == 's':
                    if self.score == 2:
                        card = self.hand.play('Q', 's')

                        # Case : no Qs card found, we have to play 2s
                        if card is None:
                            card = self.hand.play('2', 's')

                    else:
                        card = self.hand.play('2', 's')

                        # Case : no 2s card found, we have to play Qs
                        if card is None:
                            card = self.hand.play('Q', 's')
                else:
                    card = self.hand.play('2', game.declared_suit)

        else:
            # Strategy is similar to naive strategy: save up frime cards, play
            # according to suit before playing according to rank and play Qs or
            # 2 to avoid drawing cards
            # The main differences rely in:
            #     - Qs is prioritized over 2
            #     - J is prioritized over other cards
            #     - frimes are not necessarily played in suit order
            #     - when choosing a most common suit, frimes are not counted
            #     - when next player is about to score, prioritize Qs, 2, J or A
            #     - when only two players are left, save up on Qs and 2
            #
            # Those strategies have limited effects for many reasons:
            #     - The game is mostly based on luck
            #     - The strategies mentioned are rarely applicable because it
            #       often is difficult to have even one playable card. It's only
            #       more difficult to be able to choose between two or more
            #       playable cards
            #     - The player only have access to current discard_pile,
            #       therefore he cannot deduce the cards in other players'
            #       hand when a discard_pile resets nor develop a strategy based
            #       on this
            #     - The player does not remember when another played drew a
            #       card, therefore he cannot deduce which suits are missing in
            #       another player's hand based on this information
            # Also, for the effects to be visible, it is necessary that, in a
            # game of 4 or more, the order in which the players play are
            # randomized, or else the players that are not adjacent to the not
            # naive player are favored.

            # Need to play 2 or Qs, we prioritize Qs to make others draw more
            # cards
            if game.draw_count > 0:
                if no_declared_suit:
                    card = self.hand.play('Q', 's')

                    # Case : no Qs card found, we have to play 2
                    if card is None:
                        card = self.hand.play('2')

                elif game.declared_suit == 's':

                    card = self.hand.play('Q', 's')

                    # Case : no Qs card found, we have to play 2s
                    if card is None:
                        card = self.hand.play('2', 's')

                else:
                    card = self.hand.play('2', game.declared_suit)
            else:
                # Check number of cards next player have
                next_player_cards_number = -1
                for i in range(len(game.players)):
                    game.players.next()
                    if i == 0:
                        next_player_cards_number = len(game.players.peek().hand)

                # If only one card left, try to play Qs, 2, J or A to avoid
                # next player scoring
                if next_player_cards_number == 1:
                    if no_declared_suit:
                        if top_card._suit == 's':
                            card = self.hand.play('Q', 's')

                        if card is None:
                            card = self.hand.play('2', top_card._suit)

                        if card is None and top_card._rank == '2':
                            card = self.hand.play('2')

                        if card is None:
                            card = self.hand.play('J', top_card._suit)

                        if card is None and top_card._rank == 'J':
                            card = self.hand.play('J')

                        if card is None:
                            card = self.hand.play('A', top_card._suit)

                        if card is None and top_card._rank == 'A':
                            card = self.hand.play('A')

                    elif game.declared_suit == '*':

                        card = self.hand.play('Q', 's')

                        if card is None:
                            card = self.hand.play('2')

                        if card is None:
                            card = self.hand.play('J')

                        if card is None:
                            card = self.hand.play('A')
                    else:
                        if game.declared_suit == 's':
                            card = self.hand.play('Q', 's')

                        if card is None:
                            card = self.hand.play('2', game.declared_suit)

                        if card is None:
                            card = self.hand.play('J', game.declared_suit)

                        if card is None:
                            card = self.hand.play('A', game.declared_suit)

                # No card decided yet, choose card by trying to prioritize
                # J in order to have more 'J' played on others
                if card is None:

                    # Extract list of frimes so frimes aren't counted when
                    # computing most_common_suit, nor played when we aren't
                    # forced to
                    # A list of frimes couldn't be used in naive strategy
                    # because if affected most_common_suit and could therefore
                    # affect the tests
                    frime_card = self.hand.play(str(self.score))
                    while frime_card is not None:
                        frimes.add(frime_card)
                        frime_card = self.hand.play(str(self.score))

                    # Suit by which we choose our card
                    suit_arg = ''

                    if no_declared_suit:
                        suit_arg = top_card._suit
                    elif game.declared_suit == '*':
                        suit_arg = self.hand.get_most_common_suit()
                    else:
                        suit_arg = game.declared_suit

                    # Prioritize playing J
                    card = self.hand.play('J', suit_arg)

                    # Only two players left, it would be interesting to save
                    # special cards like Qs or 2 for when other player have
                    # only 1 card, because having less cards means having less
                    # chance to have a playable card
                    # Furthermore, having Qs and 2 stored up means that if the
                    # other player plays Qs or 2, we have more chance to turn
                    # it against the other player
                    if card is None and len(game.players) == 2:

                        # Special cards that were picked but not played
                        special_cards = LinkedList()

                        is_normal_card = False
                        while not is_normal_card and len(self.hand) > 0:
                            card = self.hand.play(suit_arg)

                            if card is None:
                                break
                            elif card == Card('Q', 's'):
                                special_cards.add(card)
                            elif card._rank == '2':
                                special_cards.add(card)
                            else:
                                is_normal_card = True

                            # Put special cards back
                        while not special_cards.isEmpty():
                            self.hand.add(special_cards.pop())

                    # Play according to suit
                    if card is None:
                        card = self.hand.play(suit_arg)

                    # Play according to rank
                    if card is None and game.declared_suit == '':
                        card = self.hand.play(top_card._rank)

                    # No other choice but to play frime
                    if card is None:
                        if card is None:
                            if not frimes.isEmpty():
                                card = frimes.pop()
                            else:
                                return game

        # At this point the player has chosen his card, the card is played
        # and its effect to game.declared_suit is computed

        # Case : no card
        if card is None:
            pass

        # Case : frime card
        elif card._rank == str(self.score) \
                or card._rank == 'A' and self.score == 1:
            # Player leaves the game without declaring suit
            if self.hand.__len__() == 0 and self.score == 1:
                game.discard_pile.add(card)
                game.declared_suit = '*'
            else:
                temp_suit = self.hand.get_most_common_suit()
                game.discard_pile.add(card)
                game.declared_suit = temp_suit

        # Case : normal card
        else:
            game.discard_pile.add(card)
            game.declared_suit = ''

        # Place back the frimes
        if self.strategy != 'naive':
            while not frimes.isEmpty():
                self.hand.add(frimes.pop())

        return game


class Game:
    def __init__(self):
        self.players = CircularLinkedList()

        for i in range(1, 5):
            self.players.append(Player('Player ' + str(i)))

        self.deck = Deck()
        self.discard_pile = LinkedList()

        self.draw_count = 0
        self.declared_suit = ''

    def __str__(self):
        result = '--------------------------------------------------\n'
        result += 'Deck: ' + str(self.deck) + '\n'
        result += 'Declared Suit: ' + str(self.declared_suit) + ', '
        result += 'Draw Count: ' + str(self.draw_count) + ', '
        result += 'Top Card: ' + str(self.discard_pile.peek()) + '\n'

        for player in self.players:
            result += str(player) + ': '
            result += 'Score: ' + str(player.score) + ', '
            result += str(player.hand) + '\n'
        return result

    # Puts all cards from discard pile except the
    # top card back into the deck in reverse order
    # and shuffles it 7 times
    def reset_deck(self):
        top_card = None

        # Take out top card
        if not self.discard_pile.isEmpty():
            top_card = self.discard_pile.pop()

        while not self.discard_pile.isEmpty():
            self.deck.append(self.discard_pile.pop())

        # Place top card back
        if top_card is not None:
            self.discard_pile.add(top_card)

        if len(self.deck) > 1:
            for i in range(0, 7):
                self.deck.shuffle()

    # Safe way of drawing a card from the deck
    # that resets it if it is empty after card is drawn
    def draw_from_deck(self, num):
        player = self.players.peek()
        while num > 0:
            if self.deck.isEmpty():
                self.reset_deck()
                if self.deck.isEmpty():
                    # Not enough cards in game, player draws less cards
                    return

            player.hand.add(self.deck.draw())
            num -= 1

        if self.deck.isEmpty():
            self.reset_deck()

    def start(self, debug=False):
        # Ordre dans lequel les joeurs gagnent la partie
        result = LinkedList()

        self.reset_deck()

        # Each player draws 8 cards
        for player in self.players:
            for i in range(8):
                player.hand.add(self.deck.draw())

        self.discard_pile.add(self.deck.draw())

        transcript = open('result.txt', 'w', encoding='utf-8')
        if debug:
            transcript = open('result_debug.txt', 'w', encoding='utf-8')

        while (not self.players.isEmpty()):
            if debug:
                transcript.write(str(self))

            # Boolean for if card played is J
            pass_next_player = False

            # player plays turn
            player = self.players.peek()

            old_top_card = self.discard_pile.peek()

            self = player.play(self)

            new_top_card = self.discard_pile.peek()

            # Player didn't play a card => must draw from pile
            if new_top_card == old_top_card:
                if self.draw_count != 0:
                    transcript.write(str(player) + " draws " \
                                     + str(self.draw_count) + " cards\n")
                    self.draw_from_deck(self.draw_count)
                    self.draw_count = 0
                else:
                    transcript.write(str(player) + " draws 1 card\n")
                    self.draw_from_deck(1)

            # Player played a card
            else:
                transcript.write(str(player) + " plays " \
                                 + str(new_top_card) + '\n')
                new_card_rank = new_top_card._rank
                if new_card_rank == 'A' or new_card_rank == '1':
                    self.players.reverse()
                elif new_card_rank == 'J' or new_card_rank == '11':
                    pass_next_player = True
                elif new_card_rank == '2':
                    self.draw_count += 2
                elif new_top_card == Card('Q', 's'):
                    self.draw_count += 5

            # Handling player change
            # Player has finished the game
            if len(player.hand) == 0 and player.score == 1:
                self.players.pop()
                result.append(player)
                transcript.write(str(player) + " finishes in position " \
                                 + str(len(result)) + "\n")

                # Game ends
                if len(self.players) < 2:
                    last_player = self.players.pop()
                    result.append(last_player)
                    transcript.write(str(last_player) + " finishes last")
                    return result
            else:
                # Player is out of cards to play
                if len(player.hand) == 0:
                    player.score -= 1
                    self.draw_from_deck(player.score)
                    transcript.write(str(player) + " is out of cards to play! "
                                     + str(player) + " draws " +
                                     str(player.score) + " cards\n")

                # Player has a single card left to play
                elif len(player.hand) == 1:
                    transcript.write("*Knock, knock* - " + str(player) +
                                     " has a single card left!\n")

                self.players.next()

            if pass_next_player:
                self.players.next()

        return result


if __name__ == '__main__':
    random.seed(420)
    game = Game()
    print(game.start(debug=True))

    # TESTS
    # LinkedList
    l = LinkedList()
    l.append('b')
    l.append('c')
    l.add('a')

    assert (str(l) == '[a, b, c]')
    assert (l.pop() == 'a')
    assert (len(l) == 2)
    assert (str(l.remove('c')) == 'c')
    assert (l.remove('d') == None)
    assert (str(l) == '[b]')
    assert (l.peek() == 'b')
    assert (l.pop() == 'b')
    assert (len(l) == 0)
    assert (l.isEmpty())

    # CircularLinkedList
    l = CircularLinkedList()
    l.append('a')
    l.append('b')
    l.append('c')

    assert (str(l) == '[a, b, c]')
    l.next()
    assert (str(l) == '[b, c, a]')
    l.next()
    assert (str(l) == '[c, a, b]')
    l.next()
    assert (str(l) == '[a, b, c]')
    l.reverse()
    assert (str(l) == '[a, c, b]')
    assert (l.pop() == 'a')
    assert (str(l) == '[c, b]')

    # Card
    c1 = Card('A', 's')
    c2 = Card('A', 's')
    # Il est pertinent de traiter le rang 1
    # comme étant l'ace
    c3 = Card('1', 's')
    assert (c1 == c2)
    assert (c1 == c3)
    assert (c3 == c2)

    # Hand
    h = Hand()
    h.add(Card('A', 's'))
    h.add(Card('8', 's'))
    h.add(Card('8', 'h'))
    h.add(Card('Q', 'd'))
    h.add(Card('3', 'd'))
    h.add(Card('3', 'c'))

    assert (str(h) == '[8♠, A♠][8♡][3♢, Q♢][3♣]')
    assert (str(h['d']) == '[3♢, Q♢]')
    assert (h.play('3', 'd') == Card('3', 'd'))
    assert (str(h) == '[8♠, A♠][8♡][Q♢][3♣]')
    assert (str(h.play('8')) == '8♠')
    assert (str(h.play('c')) == '3♣')
    assert (str(h) == '[A♠][8♡][Q♢][]')
    assert (h.play('d', 'Q') == Card('Q', 'd'))
    assert (h.play('1') == Card('A', 's'))
    assert (h.play('J') == None)

    # Deck
    d = Deck(custom=True)
    d.append(Card('A', 's'))
    d.append(Card('2', 's'))
    d.append(Card('3', 's'))
    d.append(Card('A', 'h'))
    d.append(Card('2', 'h'))
    d.append(Card('3', 'h'))

    random.seed(15)

    temp = copy.deepcopy(d)
    assert (str(temp) == '[A♠, 2♠, 3♠, A♡, 2♡, 3♡]')
    temp.shuffle()
    assert (str(temp) == '[A♠, A♡, 2♠, 2♡, 3♠, 3♡]')
    temp = copy.deepcopy(d)
    temp.shuffle()
    assert (str(temp) == '[A♡, A♠, 2♡, 2♠, 3♡, 3♠]')
    assert (d.draw() == Card('A', 's'))
