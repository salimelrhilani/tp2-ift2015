
begin transaction;

CREATE TABLE Segment (
	indIP numeric,
	nomSegment VARCHAR NOT NULL,
	etage numeric,
	primary key (indIP) 
);


CREATE TABLE Salle (
	nSalle numeric,
	nomSalle VARCHAR NOT NULL,
	nbPoste numeric,
	indIP numeric,
	primary key (nSalle) 
);

CREATE TABLE Poste (
	nPoste numeric,
	nomPoste VARCHAR NOT NULL,
	ad numeric(255),
	primary key (nPoste),
	foreign key (indIP) references Segment(indIP),
	foreign key (typePoste) references Types(typeLP),
	foreign key (nSalle) references Salle(nSalle)
);
CREATE TABLE Logiciel (
	nLog numeric,
	nomLog VARCHAR,
	dateAch date,
	version VARCHAR,
	typeLog VARCHAR,
	prix numeric(10) CHECK (prix >= 0),
	primary key(nLog)

);
CREATE TABLE Installer (
	numsIns serial,
	dateIns TIMESTAMP DEFAULT CURRENT_DATE,
	delai numeric,
	foreign key (nPoste) references Poste(nPoste),
	foreign key (nLog) references Logiciel(nLog),
);
CREATE TABLE Types (
	typeLP VARCHAR,
	nomType VARCHAR,
	primary key(typeLP)	
);




alter table Segment alter column indIP type VARCHAR;
alter table Salle alter column nSalle type VARCHAR;
alter table Logiciel alter column nLog type VARCHAR;
alter table Poste alter column nPoste type VARCHAR;
alter table Installer alter column dateIns drop NOT NULL;

commit;



