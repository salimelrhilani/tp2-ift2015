
begin transaction;

CREATE TABLE Segment (
	indIP INT,
	nomSegment TEXT NOT NULL,
	etage INT,
	constraint segtpk primary key (indIP)
);


CREATE TABLE Salle (
	nSalle INT,
	nomSalle TEXT NOT NULL,
	nbPoste INT,
	indIP INT,
	constraint nsalle_pk primary key (nSalle) 

);

CREATE TABLE Poste (
	nPoste INT,
	nomPoste TEXT NOT NULL,
	ad INT,
	constraint npst_pk primary key (nPoste),
	CONSTRAINT ad_0_255 CHECK(CAST(ad AS INTEGER) BETWEEN 0 AND 255)
);
CREATE TABLE Logiciel (
	nLog INT,
	nomLog TEXT,
	dateAch date,
	version TEXT,
	typeLog TEXT,
	prix INT,
	CONSTRAINT prix_positivNN CHECK(prix >= 0),
	CONSTRAINT log_pk primary key(nLog)

);
CREATE TABLE Installer (
	numsIns serial,
	dateIns TIMESTAMP DEFAULT CURRENT_DATE NOT NULL,
	delai INT
);
CREATE TABLE Types (
	typeLP TEXT,
	nomType TEXT,
	CONSTRAINT t_pk primary key(typeLP)	
);

commit;

