
begin transaction;

delete from Salle where indIP not in (select indIP from Segment),


alter table Poste add constraint poste_fk foreign key (indIP) references Segment(indIP);
alter table Poste add constraint type_fk foreign key (typePoste) references Types(typeLP);
alter table Poste add constraint nSalle_fk foreign key (nSalle) references Salle(nSalle);
alter table Installer add constraint installer_n_Poste_fk foreign key (nPoste) references Poste(nPoste);
alter table Installer add constraint nLog_fk foreign key (nLog) references Logiciel(nLog);
alter table Logiciel add constraint installer_n_Poste_fk foreign key (typeLog) references Types(typeLog);
alter table Salle add constraint inIPSalle_fk foreign key (indIP) references Segment(indIP);

-- Si on declare les contraintes referentielles dans les tables alors une table contenant une cle etrangere est vu comme 
-- la table enfant de la table contenant la cle primaire qui est donc la table parent.
-- ainsi la creation serait Segment/Salle/Types/Logiciel/Poste/Installer
-- et la supression se ferait en ordre inverse.
commit;