BEGIN TRANSACTION;

CREATE TABLE Segment (
    indIP INTEGER,
	nomSegment TEXT,
	etage INTEGER,
	CONSTRAINT pk_indIP PRIMARY KEY(indIP),
	CONSTRAINT nn_nomSegment CHECK(nomSegment IS NOT NULL)
);

CREATE TABLE Salle (
    nSalle INTEGER,
	nomSalle TEXT,
	nbPoste INTEGER,
	indIP INTEGER,
	CONSTRAINT pk_nSalle PRIMARY KEY(nSalle),
	CONSTRAINT nn_nomSalle CHECK(nomSalle IS NOT NULL)
);

CREATE TABLE Poste (
    nPoste INTEGER,
	nomPoste TEXT,
	indIP INTEGER,
	ad CHAR(3),
	typePoste TEXT,
	nSalle INTEGER,
	CONSTRAINT pk_nPoste PRIMARY KEY(nPoste),
	CONSTRAINT nn_nomPoste CHECK(nomPoste IS NOT NULL),
	CONSTRAINT ad_0_255 CHECK(CAST(ad AS INTEGER) BETWEEN 0 AND 255)
);

CREATE TABLE Logiciel (
    nLog INTEGER,
	nomLog TEXT,
	dateAch DATE,
	version TEXT,
	typeLog TEXT,
	prix REAL,
	CONSTRAINT pk_nLog PRIMARY KEY(nLog),
	CONSTRAINT prix_gt_0 CHECK(prix >= 0)
);

CREATE TABLE Installer (
    numIns SERIAL,
	nPoste INTEGER,
	nLog INTEGER,
	dateIns TIMESTAMP DEFAULT CURRENT_DATE,
	delai INTEGER,
	CONSTRAINT pk_numIns PRIMARY KEY(numIns),
	CONSTRAINT nn_dateIns CHECK(dateIns IS NOT NULL)
);

CREATE TABLE Types (
    typeLP TEXT,
	nomType TEXT,
	CONSTRAINT pk_typeLP PRIMARY KEY(typeLP)
);


COMMIT;