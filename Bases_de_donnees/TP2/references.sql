BEGIN TRANSACTION;

DELETE FROM Salle WHERE (indIP NOT IN (SELECT indIP FROM Segment));

ALTER TABLE Poste
ADD CONSTRAINT fk_indIP_Segment FOREIGN KEY(indIP) REFERENCES Segment(indIP),
ADD CONSTRAINT fk_typePoste_Types FOREIGN KEY(typePoste) REFERENCES Types(typeLP),
ADD CONSTRAINT fk_nSalle_Salle FOREIGN KEY(nSalle) REFERENCES Salle(nSalle);

ALTER TABLE Installer
ADD CONSTRAINT fk_nPoste_Poste FOREIGN KEY(nPoste) REFERENCES Poste(nPoste),
ADD CONSTRAINT fk_nLog_Logiciel FOREIGN KEY(nLog) REFERENCES Logiciel(nLog);

ALTER TABLE Salle
ADD CONSTRAINT fk_indIP_Segment FOREIGN KEY(indIP) REFERENCES Segment(indIP);

-- Si on avait ajouté ces contraintes référentielles dès le début, alors il 
-- faudrait créer les tables mères avant les tables filles et supprimer les 
-- tables filles avant les tables mères.

-- Donc, un ordre de création possible serait Segment, Salle, Types, Logiciel, 
-- Poste, Installer et l'ordre de suppression serait Installer, Poste, 
-- Logiciel, Types, Salle, Segment.

COMMIT;